import sqlite3

try:
  conn = sqlite3.connect('database.db')
  c = conn.cursor()
  c.execute("""CREATE TABLE Personeel(Naam text, Telefoon text, Email text)""")
  conn.commit()
  conn.close()
  print("New database created\n\n")
except:
  print("Database is available\n\n")

def invoer():
  name = input("Naam: ")
  phone = input("Telefoon: ")
  email = input("Email adres: ")
   
  conn = sqlite3.connect('database.db')
  c = conn.cursor()
  c.execute("INSERT INTO Personeel (Naam, Telefoon, Email) VALUES (?, ?, ?)", (name, phone, email))
  conn.commit()
  conn.close()
  menu2()

def zoek():
  name = input("Op welke naam wil je zoeken? ")
  conn = sqlite3.connect('database.db')
  c = conn.cursor()
  searchdb = """SELECT * FROM Personeel WHERE naam = '%s'"""%(name)
  c.execute(searchdb)
  info = c.fetchall()
  for row in info:
    print("Naam: ", row[0],"\nTelefoon: ", row[1],"\nEmailadres: ", row[2],"\n")
  conn.commit()
  conn.close()
  menu2()

def alles():
  conn = sqlite3.connect('database.db')
  c = conn.cursor()
  searchdb = """SELECT * FROM Personeel"""
  c.execute(searchdb)
  info = c.fetchall()
  for row in info:
    print("Naam: ", row[0],"\nTelefoon: ", row[1],"\nEmailadres: ", row[2],"\n")
  conn.commit()
  conn.close()
  menu2()

def verwijder():
  verw = input("Welke naam wil je verwijderen? ")
  conn = sqlite3.connect('database.db')
  c = conn.cursor()
  verwdb = """DELETE FROM Personeel Where Naam = '%s'"""%(verw)
  c.execute(verwdb)
  conn.commit()
  conn.close()
  menu2()

def menu():
  print("Welkom bij weer een nieuwe SQL/Python database test omgeving.\nNaam telefoon en email gegevens invoeren opzoeken verwijderen\nmoet natuurlijk getest en begrepen worden.\nHieronder volgt de uitvoering van dat wat reeds geprogrammeerd is.\n")
  print("Wat wil je doen?\n\n1 = gegevens invoeren\n2 = gegevens zoeken\n3 = laat alles zien\n4 = gegevens verwijderen\n5 = Programma stoppen\n")
  ingave = input("Maak een keuze: ")
  print("\n\n")

  if ingave == "1":
    invoer()
  elif ingave == "2":
    zoek()
  elif ingave == "3":
    alles()
  elif ingave == "4":
    verwijder()
  elif ingave == "5":
    print("Einde van het programma.")
  elif ingave != "1" or "2" or "3" or "4" or "5":
    print("Graag een juiste keuze maken. Dus je typt 1, 2, 3, 4 of 5 en drukt op enter")
    menu2()
      
def menu2():
  print("\n\nWat wil je verder nog doen?\n\n1 = gegevens invoeren\n2 = gegevens zoeken\n3 = laat alles zien\n4 = gegevens verwijderen\n5 = Programma stoppen\n")
  ingave = input("Maak een keuze: ")
  print("\n\n")

  if ingave == "1":
    invoer()
  elif ingave == "2":
    zoek()
    menu2()
  elif ingave == "3":
    alles()
    menu2()
  elif ingave == "4":
    verwijder()
    menu2()
  elif ingave == "5":
    print("\nEinde van het programma.")
          
  elif ingave != "1" or "2" or "3" or "4" or "5":
    print("Graag een juiste keuze maken. Dus je typt 1, 2 of 3 en drukt op enter")
    menu2()
menu()